#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "NastyaDux/Weapon/BaseWeapon.h"
#include "WeaponComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NASTYADUX_API UWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:		
	UWeaponComponent();

	void StartFire();
	void StopFire();		
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputMappingContext* FireMappingContext;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* FireAction;

protected:
	UPROPERTY(EditDefaultsOnly, Category = Weapon)
	TSubclassOf<ABaseWeapon>WeaponClass;
	UPROPERTY(EditDefaultsOnly, Category = Weapon)
	FName WeaponAttachPointName = "WeaponSocket";	
	
	virtual void BeginPlay() override;	

private:
	UPROPERTY()
	ABaseWeapon* CurrentWeapon = nullptr;

	void AttachWeapon();
};
