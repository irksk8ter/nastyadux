#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "NastyaDux/Components/WeaponComponent.h"
#include "NastyaDuxCharacter.generated.h"

UCLASS(Blueprintable)
class ANastyaDuxCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ANastyaDuxCharacter();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
	UWeaponComponent* WeaponComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Aim")
	bool bIsAiming;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	float DefaultWalkSpeed;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
	float SpeedMultiplier;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
};
