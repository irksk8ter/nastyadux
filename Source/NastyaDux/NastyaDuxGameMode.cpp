#include "NastyaDuxGameMode.h"
#include "NastyaDuxPlayerController.h"
#include "NastyaDuxCharacter.h"
#include "UObject/ConstructorHelpers.h"

ANastyaDuxGameMode::ANastyaDuxGameMode()
{	
	PlayerControllerClass = ANastyaDuxPlayerController::StaticClass();
	
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Characters/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Core/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}
