
#include "NastyaDuxPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraFunctionLibrary.h"
#include "NastyaDuxCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Components/DecalComponent.h"
#include "Engine/LocalPlayer.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

ANastyaDuxPlayerController::ANastyaDuxPlayerController()
{	
	PrimaryActorTick.bCanEverTick = true;
	//Mouse
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	RotationSpeed = 100.0f;	
	//Movement
	SprintSpeedMultiplier = 2.0f;
	WalkSpeedMultiplier = 0.5f;
	//Camera
	MinCameraDistance = 600.0f;
	MaxCameraDistance = 1500.0f;
	TargetArmLength = 1200.0f;
	ZoomSpeed = 5.0f;
	MaxCameraOffset = 200.0f;	
}

void ANastyaDuxPlayerController::BeginPlay()
{	  
	Super::BeginPlay();	
	
	ControlledCharacter = Cast<ANastyaDuxCharacter>(GetPawn());
	//Camera
	APawn* ControlPawn = GetPawn();
	if (ControlPawn)
	{
		CameraBoom = ControlPawn->FindComponentByClass<USpringArmComponent>();
	}

	if (!CursorMaterial)
	{
		// ������ ��������� ������ � ����������� ��� �������� �������
		CurrentCursor = NewObject<UDecalComponent>(this);
		if (CurrentCursor)
		{
			CurrentCursor->SetupAttachment(RootComponent);
			CurrentCursor->SetDecalMaterial(CursorMaterial);
			CurrentCursor->DecalSize = CursorSize;
			CurrentCursor->RegisterComponent();
		}
	}
}

void ANastyaDuxPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	RotatePawnToMouseCursor(DeltaTime);
	//UpdateCameraPosition(DeltaTime);

	//Camera (������� ������������ ���� ������)
	if (CameraBoom)
	{
		float CurrentArmLength = CameraBoom->TargetArmLength;
		CameraBoom->TargetArmLength = FMath::FInterpTo(CurrentArmLength, TargetArmLength, DeltaTime, ZoomSpeed);
	}

	if (CurrentCursor)
	{
		FHitResult HitResult;
		GetHitResultUnderCursor(ECC_Visibility, true, HitResult);

		// ������������� �������������� � ������� ������ � ����� �����
		if (HitResult.IsValidBlockingHit())
		{
			CurrentCursor->SetWorldLocation(HitResult.Location);
			CurrentCursor->SetWorldRotation(HitResult.ImpactNormal.Rotation());
		}
	}	
}

void ANastyaDuxPlayerController::SetupInputComponent()
{	
	Super::SetupInputComponent();
	
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}

	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
	{
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ANastyaDuxPlayerController::Move);

		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Started, this, &ANastyaDuxPlayerController::StartSprinting);
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Completed, this, &ANastyaDuxPlayerController::StopSprinting);

		EnhancedInputComponent->BindAction(WalkAction, ETriggerEvent::Started, this, &ANastyaDuxPlayerController::StartWalking);
		EnhancedInputComponent->BindAction(WalkAction, ETriggerEvent::Completed, this, &ANastyaDuxPlayerController::StopWalking);

		EnhancedInputComponent->BindAction(ZoomInAction, ETriggerEvent::Triggered, this, &ANastyaDuxPlayerController::ZoomIn);
		EnhancedInputComponent->BindAction(ZoomOutAction, ETriggerEvent::Triggered, this, &ANastyaDuxPlayerController::ZoomOut);

		EnhancedInputComponent->BindAction(AimAction, ETriggerEvent::Triggered, this, &ANastyaDuxPlayerController::StartAiming);
		EnhancedInputComponent->BindAction(AimAction, ETriggerEvent::Completed, this, &ANastyaDuxPlayerController::StopAiming);		
	}

	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input Component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}



void ANastyaDuxPlayerController::Move(const FInputActionValue& Value)
{
	// Input is a Vector2D (X, Y)
	FVector2D MovementVector = Value.Get<FVector2D>();

	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn)
	{
		// ��������������� ��������� ����������, ����� �� ���� ��������� � ������ ������
		FRotator NewControlRotation = GetControlRotation();
		FRotator YawRotation(0, NewControlRotation.Yaw, 0);

		// ������� �������� ������ � ������ ������������ ������
		FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// ��������� ��������
		ControlledPawn->AddMovementInput(ForwardDirection, MovementVector.Y);
		ControlledPawn->AddMovementInput(RightDirection, MovementVector.X);
	}
}

void ANastyaDuxPlayerController::StartSprinting()
{	
	APawn* ControlledPawn = GetPawn(); 
	if (ControlledPawn)
	{
		ACharacter* LocalControlledCharacter = Cast<ACharacter>(ControlledPawn); 
		if (LocalControlledCharacter)
		{
			LocalControlledCharacter->GetCharacterMovement()->MaxWalkSpeed *= SprintSpeedMultiplier;
		}
	}
}

void ANastyaDuxPlayerController::StopSprinting()
{
	APawn* ControlledPawn = GetPawn(); 
	if (ControlledPawn)
	{
		ACharacter* LocalControlledCharacter = Cast<ACharacter>(ControlledPawn); 
		if (LocalControlledCharacter)
		{
			LocalControlledCharacter->GetCharacterMovement()->MaxWalkSpeed /= SprintSpeedMultiplier;
		}
	}
}

void ANastyaDuxPlayerController::StartWalking()
{
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn)
	{
		ACharacter* LocalControlledCharacter = Cast<ACharacter>(ControlledPawn);
		if (LocalControlledCharacter)
		{
			LocalControlledCharacter->GetCharacterMovement()->MaxWalkSpeed *= WalkSpeedMultiplier;
		}
	}
}

void ANastyaDuxPlayerController::StopWalking()
{
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn)
	{
		ACharacter* LocalControlledCharacter = Cast<ACharacter>(ControlledPawn);
		if (LocalControlledCharacter)
		{
			LocalControlledCharacter->GetCharacterMovement()->MaxWalkSpeed /= WalkSpeedMultiplier;
		}
	}
}

void ANastyaDuxPlayerController::ZoomIn()
{
	TargetArmLength = FMath::Clamp(TargetArmLength - 100.0f, MinCameraDistance, MaxCameraDistance);
}

void ANastyaDuxPlayerController::ZoomOut()
{
	TargetArmLength = FMath::Clamp(TargetArmLength + 100.0f, MinCameraDistance, MaxCameraDistance);
}

//void ANastyaDuxPlayerController::UpdateCameraPosition(float DeltaTime)
//{
//	APawn* ControlledPawn = GetPawn();
//	if (!ControlledPawn) return;
//
//	// �������� ������� ���������
//	FVector PawnLocation = ControlledPawn->GetActorLocation();
//
//	// �������� ������� ������� �� ������
//	FVector2D MousePosition;
//	FVector2D ViewportSize;
//	if (GetMousePosition(MousePosition.X, MousePosition.Y) && GEngine->GameViewport)
//	{
//		GEngine->GameViewport->GetViewportSize(ViewportSize);
//	}
//
//	// ����������� ������� ������� � ������� ����������
//	FVector WorldMouseLocation, WorldMouseDirection;
//	DeprojectScreenPositionToWorld(MousePosition.X, MousePosition.Y, WorldMouseLocation, WorldMouseDirection);
//
//	// ���������� ���������� �� ������ ������ �� ������� ����
//	FVector2D ViewportCenter = ViewportSize * 0.5f;
//	FVector2D CursorOffset = MousePosition - ViewportCenter;
//
//	// ����������� �������� (����� ��� ���� ���������� �� ���� ������������)
//	//float MaxCameraOffset = 300.f;  // ������������ ���������� ��� �������� ������
//	if (CursorOffset.Size() > 0.0f)
//	{
//		CursorOffset = CursorOffset.GetSafeNormal() * FMath::Min(CursorOffset.Size(), MaxCameraOffset);
//	}
//
//	// �������� SpringArmComponent, � �������� ����������� ������
//	USpringArmComponent* SpringArm = ControlledPawn->FindComponentByClass<USpringArmComponent>();
//	if (!SpringArm) return;
//
//	// ������������ ����� �������� ������
//	FVector CameraOffset = FVector(CursorOffset.X, CursorOffset.Y, 0.0f);
//
//	// ��������� ����� ������� ������ ������������ ���������
//	FVector CameraTargetLocation = PawnLocation + CameraOffset;
//
//	// ������ ������������� ������� ������� SpringArm � ������� �������
//	FVector CurrentCameraLocation = SpringArm->GetComponentLocation();
//	FVector NewCameraLocation = FMath::VInterpTo(CurrentCameraLocation, CameraTargetLocation, DeltaTime, 5.0f);
//
//	// ������������� ����� ������� SpringArm
//	SpringArm->SetWorldLocation(NewCameraLocation);
//}

void ANastyaDuxPlayerController::RotatePawnToMouseCursor(float DeltaTime)
{	
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn == nullptr)
	{

		UE_LOG(LogTemp, Warning, TEXT("ControlledPawn is null"));
		return;
	}
	
	FVector PawnLocation = ControlledPawn->GetActorLocation();
	FHitResult HitResult;
	if (GetHitResultUnderCursor(ECC_Visibility, true, HitResult))
	{
		FVector CursorLocation = HitResult.Location;
		FRotator TargetRotation = UKismetMathLibrary::FindLookAtRotation(PawnLocation, CursorLocation);
		TargetRotation.Pitch = 0.0f;
		TargetRotation.Roll = 0.0f;			

		// ����� ���������� CurrentRotation ������ ����
		FRotator CurrentRotation = ControlledPawn->GetActorRotation();

		float RotationDifference = FMath::Abs(TargetRotation.Yaw - CurrentRotation.Yaw);		

		FRotator NewRotation = FMath::RInterpTo(CurrentRotation, TargetRotation, DeltaTime, RotationSpeed);
		ControlledPawn->SetActorRotation(NewRotation);
	}	
}

void ANastyaDuxPlayerController::StartAiming()
{
	APawn* PlayerPawn = GetPawn();
	ANastyaDuxCharacter* MyCharacter = Cast<ANastyaDuxCharacter>(PlayerPawn);
	if (MyCharacter)
	{
		MyCharacter->bIsAiming = true;  // ���������� ������������
	}
}

void ANastyaDuxPlayerController::StopAiming()
{
	APawn* PlayerPawn = GetPawn();
	ANastyaDuxCharacter* MyCharacter = Cast<ANastyaDuxCharacter>(PlayerPawn);
	if (MyCharacter)
	{
		MyCharacter->bIsAiming = false;  // ������������ ������������
	}
}
