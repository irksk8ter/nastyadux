#pragma once

#include "CoreMinimal.h"
#include "InputAction.h"
#include "NiagaraSystem.h"
#include "NastyaDuxCharacter.h"
#include "InputMappingContext.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PlayerController.h"
#include "NastyaDux/Components/WeaponComponent.h"
#include "Materials/MaterialInterface.h"
#include "NastyaDuxPlayerController.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTemplateCharacter, Log, All);

UCLASS()
class ANastyaDuxPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ANastyaDuxPlayerController();

	UDecalComponent* CurrentCursor = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Cursor")
	UMaterialInterface* CursorMaterial = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	USpringArmComponent* CameraBoom;	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;	
	//BindAction
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* MoveAction;	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* SprintAction;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* WalkAction;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	UInputAction* ZoomInAction;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	UInputAction* ZoomOutAction;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	UInputAction* AimAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float RotationSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float SprintSpeedMultiplier;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float WalkSpeedMultiplier;	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	float MinCameraDistance;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	float MaxCameraDistance;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	float TargetArmLength;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	float ZoomSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	float MaxCameraOffset;

protected:	
	uint32 bMoveToMouseCursor : 1;		
	
	virtual void BeginPlay();
	virtual void Tick(float DeltaTime) override;

	virtual void SetupInputComponent() override;
	//Movement
	void Move(const FInputActionValue& Value);
	void StartSprinting();
	void StopSprinting();
	void StartWalking();
	void StopWalking();
	//Camera
	void ZoomIn();
	void ZoomOut();
	//void UpdateCameraPosition(float DeltaTime);
	//Mouse
	void RotatePawnToMouseCursor(float DeltaTime);
	void StartAiming();
	void StopAiming();
	
private:
	FVector CachedDestination;
	ANastyaDuxCharacter* ControlledCharacter;	
};
