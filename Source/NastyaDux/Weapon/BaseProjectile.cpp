#include "NastyaDux/Weapon/BaseProjectile.h"

ABaseProjectile::ABaseProjectile()
{ 	
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	BulletCollisionSphere->SetSphereRadius(16.f);
	BulletCollisionSphere->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	BulletCollisionSphere->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	BulletCollisionSphere->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);
	BulletCollisionSphere->SetCollisionResponseToAllChannels(ECR_Block);
	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &ABaseProjectile::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ABaseProjectile::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &ABaseProjectile::BulletCollisionSphereEndOverlap);
	BulletCollisionSphere->bReturnMaterialOnMove = true;
	BulletCollisionSphere->SetCanEverAffectNavigation(false);	

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);
	BulletMesh->SetNotifyRigidBodyCollision(false);		
	BulletMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);	

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet Projectile Movement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 1.0f;
	BulletProjectileMovement->MaxSpeed = 0.0f;
	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = false;
}

void ABaseProjectile::BeginPlay()
{
	Super::BeginPlay();
	
	check(BulletCollisionSphere);
	check(BulletMesh);
	check(BulletFX);
	check(BulletProjectileMovement);
}

void ABaseProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseProjectile::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// ��������, ��� ������ ���������� � ������ ������� � ��� ��� �� ��� ������
	if (OtherActor && OtherActor != this)
	{
		// ����������� ������� ��� ������������
		UE_LOG(LogTemp, Warning, TEXT("Projectile hit: %s"), *OtherActor->GetName());
		Destroy();
	}
}

void ABaseProjectile::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void ABaseProjectile::BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

