#include "NastyaDux/Weapon/BaseWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "NastyaDux/NastyaDuxPlayerController.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogBaseWeapon, All, All);

ABaseWeapon::ABaseWeapon()
{
	PrimaryActorTick.bCanEverTick = true;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>("WeaponMesh");
	SetRootComponent(WeaponMesh);

    MuzzleFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("MuzzleFX"));
    MuzzleFX->SetupAttachment(WeaponMesh, "Muzzle"); // ����������� � ������ Muzzle �� WeaponMesh
    MuzzleFX->bAutoActivate = false; // ������ �� ������������ �������������
}

void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	
	check(WeaponMesh);
}

void ABaseWeapon::StartFire()
{	

}

void ABaseWeapon::StopFire()
{
	
}

void ABaseWeapon::MakeShot()
{
   
}
