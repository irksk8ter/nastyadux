#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "NastyaDux/Weapon/BaseProjectile.h"
#include "BaseWeapon.generated.h"

UCLASS()
class NASTYADUX_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:		
	ABaseWeapon();

	virtual void StartFire();
	virtual void StopFire();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon");
	class UParticleSystemComponent* MuzzleFX = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	USkeletalMeshComponent* WeaponMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon")
	FName MuzzleSocketName = "Muzzle";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	USoundBase* FireSound;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon")
	float TraceMaxDistance = 1500.0f;	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float FireRate = 0.1f; // ����� ����� ���������� � ��������
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float BulletSpread = 1.5f;

	virtual void BeginPlay() override;	
	virtual void MakeShot();
};
