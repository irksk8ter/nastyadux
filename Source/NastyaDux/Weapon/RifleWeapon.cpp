#include "NastyaDux/Weapon/RifleWeapon.h"
#include "Kismet/GameplayStatics.h"
#include "NastyaDux/NastyaDuxPlayerController.h"
#include "Engine/World.h"

DEFINE_LOG_CATEGORY_STATIC(LogBaseWeapon, All, All);

void ARifleWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DrawLaserSight();
}

void ARifleWeapon::StartFire()
{	
	if (!FireRate /*> 0.f*/)
	{
		MakeShot();
	}
	else
	{
		GetWorldTimerManager().SetTimer(FireTimerHandle, this, &ARifleWeapon::MakeShot, FireRate, true);
	}
}

void ARifleWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(FireTimerHandle);
	if (MuzzleFX)
	{
		MuzzleFX->DeactivateSystem(); // ��������� ������
	}
}

void ARifleWeapon::MakeShot()
{
	if (!GetWorld()) return;

	const auto Player = Cast<ACharacter>(GetOwner());
	if (!Player) return;

	const auto Controller = Player->GetController<ANastyaDuxPlayerController>();
	if (!Controller) return;

	if (!WeaponMesh->DoesSocketExist(MuzzleSocketName))
	{
		UE_LOG(LogTemp, Warning, TEXT("Socket %s does not exist on WeaponMesh!"), *MuzzleSocketName.ToString());
		return;
	}

	const FTransform SocketTransform = WeaponMesh->GetSocketTransform(MuzzleSocketName);
	const FVector TraceStart = SocketTransform.GetLocation();
	const FVector TraceDirection = FMath::VRandCone(SocketTransform.GetRotation().GetForwardVector(), FMath::DegreesToRadians(BulletSpread));
	const FVector TraceEnd = TraceStart + TraceDirection * TraceMaxDistance;

	//DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, false, 1.0f, 0, 1.0f);

	FHitResult HitResult;
	GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility);

	if (HitResult.bBlockingHit)
	{
		//DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 10.0f, 24, FColor::Red, false, 5.0f);
	}

	// ����� ������� � ������������
	SpawnProjectile(TraceStart, TraceDirection.Rotation(), TraceDirection);

	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if (!Character) return;

	if (FireSound != nullptr)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, Character->GetActorLocation());
	}

	if (!WeaponMesh->DoesSocketExist("Muzzle"))
	{
		UE_LOG(LogBaseWeapon, Warning, TEXT("Socket Muzzle does not exist on WeaponMesh!"));
		return;
	}

	if (MuzzleFX)
	{
		MuzzleFX->ActivateSystem(); // ���������� ������
	}
	else
	{
		UE_LOG(LogBaseWeapon, Warning, TEXT("MuzzleFX particle component is not set!"));
	}
}

void ARifleWeapon::DrawLaserSight()
{
	if (!GetWorld()) return;

    const FTransform SocketTransform = WeaponMesh->GetSocketTransform(MuzzleSocketName);
    const FVector TraceStart = SocketTransform.GetLocation();
    const FVector TraceDirection = SocketTransform.GetRotation().GetForwardVector();
    const FVector TraceEnd = TraceStart + TraceDirection * LaserMaxDistance;  // LaserMaxDistance � ��������� ������

    // ����������� �� �������
    FHitResult HitResult;
    bool bHit = GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility);

    FVector LaserEndPoint = TraceEnd;  // ���������� �������� ����� �� ������������ ���������

    if (bHit)
    {
        LaserEndPoint = HitResult.ImpactPoint;  // ��������� �������� ����� �� ����� ������������
    }

    // ������ ����� �� ������ �� �������� ����� (��� ����� ������������)
    DrawDebugLine(GetWorld(), TraceStart, LaserEndPoint, FColor::Red, false, 0.0f, 0, 0.5f);
}

void ARifleWeapon::SpawnProjectile(const FVector& SpawnLocation, const FRotator& SpawnRotation, const FVector& ShootDirection)
{
	if (!ProjectileClass) return;

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	SpawnParams.Instigator = Cast<APawn>(GetOwner());

	// ����� �������
	ABaseProjectile* Projectile = GetWorld()->SpawnActor<ABaseProjectile>(
		ProjectileClass,
		SpawnLocation,
		SpawnRotation,
		SpawnParams
	);

	if (Projectile)
	{
		// ��������� ����������� � �������� �������
		Projectile->BulletProjectileMovement->Velocity = ShootDirection * Projectile->BulletProjectileMovement->InitialSpeed;
		Projectile->ProjectileSetting.ProjectileDamage = 20.0f; // ��������� �����
	}
}
