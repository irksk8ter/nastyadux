#pragma once

#include "CoreMinimal.h"
#include "NastyaDux/Weapon/BaseWeapon.h"
#include "RifleWeapon.generated.h"

UCLASS()
class NASTYADUX_API ARifleWeapon : public ABaseWeapon
{
	GENERATED_BODY()
	
public:
	void Tick(float DeltaTime) override;
	virtual void StartFire() override;
	virtual void StopFire() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<ABaseProjectile> ProjectileClass;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon");
	class UParticleSystemComponent* MazzleFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Laser")
	float LaserMaxDistance = 1000.0f;  // ���������� �������� �� ���������, ��������, 1000.0f	

protected:
	virtual void MakeShot() override;
	void DrawLaserSight();

private:
	// ������� ��� ������ �������
	void SpawnProjectile(const FVector& SpawnLocation, const FRotator& SpawnRotation, const FVector& ShootDirection);

	FTimerHandle FireTimerHandle; // ������ ��� ��������
};
